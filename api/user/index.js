var express = require('express');
var controller = require('./user.controller');

var router = express.Router();

router.post('/', controller.createUser);
router.get('/food', controller.showAllFood);
router.post('/food', controller.createFood);
router.delete('/food', controller.deleteFood);
router.post('/submit', controller.submitFood);
router.get('/showCustomerOrder', controller.showCustomerOrder);
router.post('/sendCustomerOrder', controller.sendCustomerOrder);
router.post('/food/today', controller.createFoodToday);
router.delete('/food/today', controller.deleteFoodToday);

module.exports = router;