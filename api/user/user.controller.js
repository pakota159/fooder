var User = require('./user.model');
var path = require('path');
var decode = require('../auth/decode');

module.exports = {
	
	createUser: (function(req, res){
		var user = new User();
		user.name = req.body.name;
		user.username = req.body.username;
		user.password = req.body.password;
		user.role = req.body.role;

		user.save(function(err){
			if(err){
				if(err.code == 11000){
					return res.json({success: false, message: 'user exist'});
				}else{
					return res.send(err);
				}
			}else{
				res.json({message: 'User created'});
			}
		});
	}),

	showAllFood: (function(req, res){
		var idUser = decode.decodeUser(req);
		User.findOne({_id : idUser},function(err, user){
			if(err) res.send(err);
			res.json(user);
		});
	}),

	createFood: (function(req, res){
		var idUser = decode.decodeUser(req);
		User.findByIdAndUpdate(idUser, {$push: {"listfood": {foodname: req.body.foodname}}},
    	{safe: true, upsert: true},function(err){
			if(err) throw(err);
			res.json({message: 'create success'});
		});
	}),

	createFoodToday: function(req, res){
		var idUser = decode.decodeUser(req);
		User.findByIdAndUpdate(idUser, {$push: {"listFoodToday": {foodNameToday: req.body.foodNameToday, _id: req.body.idFoodToday}}},
    	{safe: true, upsert: true},function(err){
			if(err) throw(err);
			res.json({message: 'create food today success'});
		});
	},

	deleteFoodToday: function(req, res){
		var idUser = decode.decodeUser(req);
		User.findByIdAndUpdate(idUser, {$pull: {"listFoodToday": {_id: req.body.idFoodToday}}},
    	{safe: true, upsert: true},function(err){
			if(err) throw(err);
			res.json({message: "deleted food today success"});
		})
	},

	deleteFood: (function(req, res){
		var idUser = decode.decodeUser(req);
		User.findByIdAndUpdate(idUser, {$pull: {"listfood": {_id: req.body._id}}},
    	{safe: true, upsert: true},function(err){
			if(err) throw(err);
			User.findByIdAndUpdate(idUser, {$pull: {"listFoodToday": {_id: req.body._id}}},
	    	{safe: true, upsert: true},function(err){
				if(err) throw(err);
			})
			res.json({message: "deleted food success"});
		})
	}),


	//send Today List Food to customer
	submitFood: (function(req, res){
		var idUser = decode.decodeUser(req);
		User.findOne({_id : idUser},function(err, user){
			if(err) throw(err);
			var resListFoodToday = user.listFoodToday;
			function updateUser(cb){
				User.update({role : "customer"}, { $set: { listfood: []}},{safe: true, upsert: true, multi: true},function(){
					for (var i = 0; i < resListFoodToday.length; i++){
						User.update({role : "customer"}, {$push: {"listfood": {foodname: resListFoodToday[i].foodNameToday}}},
				    	{safe: true, upsert: true, multi: true},function(err){
							if(err) throw(err);
						});
					};
					cb();
				});
			};
			updateUser(function(){
				User.update({_id : idUser},{$set : {listFoodToday: []}}, function(err, affected){
              		if(err) throw(err);
              		res.json({ message : affected});
              	});
			});
		});
		
	}),

	//show customer order
	showCustomerOrder: (function(req, res){
		var idUser = decode.decodeUser(req);
		User.findOne({_id : idUser},function(err, user){
			if(err) res.send(err);
			res.json(user.customerOrderList);
		});
	}),

	sendCustomerOrder: (function(req, res){
		var idUser = decode.decodeUser(req);
		User.findOne({_id : idUser},function(err, user){
			var customerName = user.name;
			var chosenListToRestaurant = user.listFoodToday;
			User.findOne({role: "restaurant"}, function(err, user){
				if(err) throw(err);
				var idRestaurant = user._id;
					function deleteElement(cb){
						user.customerOrderList.forEach(function(value, index){
							if(value.ownerOrder === customerName){
								user.customerOrderList.pull(value);
							};
						});
						cb();
					};
					deleteElement(function(){
					user.save(function(err, data){
                      if(err) throw(err);

                      	User.update({_id: idRestaurant}, {$push: {"customerOrderList":{
							ownerOrder: customerName
						}}}, {safe: true, upsert: true, multi: true},function(err){
								if(err) throw(err);
								User.findById(idRestaurant, function(err, user){
								if(err) throw(err);
			                      function push(cb){
			                        chosenListToRestaurant.forEach(function(value, index){
			                          user.customerOrderList[user.customerOrderList.length - 1].listOrder.push(value.foodNameToday);
			                        });
			                        cb();
			                      };
			                      push(function(){
			                        user.save(function(err, data){
			                          if(err) throw(err);
			                          	User.update({_id : idUser},{$set : {listFoodToday: []}}, function(err, affected){
			                          		res.json({message: affected});
			                          	});
			                        });
			                      })
								});
							});

	                    });

					});
				});	
			});
				
	})
};