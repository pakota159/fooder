//define variables
var mongoose = require('mongoose');
var crypto = require('crypto');
var Schema = mongoose.Schema;
var shortId = require('shortid');
var bcrypt = require('bcrypt-nodejs');

// create user Schema
var UserSchema = new Schema({
	_id: {type: String, unique: true, 'default': shortId.generate},
	name: String,
	username: {type: String, required: true, index: {unique: true}},
	password: {type: String, required: true, select: false},
	role: {type: String, required: true},
	listfood: [{
		foodname: {type: String}
	}],
	listFoodToday:[{
		foodNameToday: {type: String}
	}],
	customerOrderList: [{
		ownerOrder: {type: String},
		listOrder: {type: Array}
	}]
});

//encrypt password
UserSchema.pre('save', function(next){
	var user = this;
	//only encrypt if new password or modified
	if (!user.isModified('password')) return next();

	bcrypt.hash(user.password, null, null, function(err, hash){
		if (err) return next(err);
		user.password = hash;
		next();
	});
});

UserSchema.methods = {comparePass : (function(password){
  	var user = this;
  	return bcrypt.compareSync(password, user.password);
  })
};

module.exports = mongoose.model('User', UserSchema);