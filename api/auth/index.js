'use strict';

var express = require('express');
var User = require('../user/user.model');

var router = express.Router();

router.use('/local', require('./local'));//Define the route to /api/auth/local

module.exports = router;