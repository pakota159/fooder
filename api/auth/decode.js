'use strict';

var jwt = require('jsonwebtoken');
var path = require('path');

module.exports = {
	decodeUser : (function(req){
		var token = req.cookies.token;
		var decodedToken = jwt.decode(token, {complete: true}).payload;
		return decodedToken;
	})
}