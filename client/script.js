function login(event){
	var username = $("#username").val();
	var password = $("#password").val();
	$.ajax({
    url: "/api/auth/local",//POST data login to server for authentication
    method: "POST",
    contentType : "application/json",
    data: JSON.stringify({"username": username, "password": password})
  }).done(function(response) {
    if (response.token) {
      console.log(response.token);
      document.cookie = "token=" + response.token;
      if(response.referrer){
        console.log(response.referrer);
        window.location.href = response.referrer;
      }else{
        console.log(response);
      };
      
    }
  });
};