//Refresh Page ========================================================
//Load All Food List and Today Food List

var refreshPage = function(){
		$('#listFood').empty();
		$('#orderTable tbody').empty();
		$.ajax({//show all food whenever get /api/user/food
		url:"/api/user/food",
		method : "GET",
		contentType: "application/json"
	}).done(function(response){
		console.log(response);
		for (var i=0; i < response.listfood.length; i++){
			var nameoffood = response.listfood[i].foodname;
			var idFood = response.listfood[i]._id;
			addFood(nameoffood, idFood);
		};
		for (var j=0; j < response.listFoodToday.length; j++){
			var foodNameToday = response.listFoodToday[j].foodNameToday;
			var idFoodToday = response.listFoodToday[j]._id;
			addFoodTable(foodNameToday, idFoodToday);
			$("#listFood *[data-id=" + idFoodToday + "] > div").css("background-color", "#26a69a");
		};
	});
};
refreshPage();
//======================================================================


//Add food to All Food List ============================================
function addFood(nameoffood, idFood){
	var tableContent = [];
	tableContent += '<div data-id="' + idFood + '" class="col s12 l3">';
	tableContent += '<div class="card">';
	tableContent += '<div class="card-content white-text">';
    tableContent += '<span class="card-title"> ' + nameoffood + '</span>';
    tableContent += '</div>';
    tableContent += '<div class="card-action">';
    tableContent += '<a class="addFoodToday"><i class="material-icons">add</i></a>';
    tableContent += '<a class="clearFoodToday"><i class="material-icons">clear</i></a>';
    tableContent += '</div>';
    tableContent += '</div>';
    tableContent += '</div>';
    $('#listFood').append(tableContent);   
};
//Submit Food to server
function submitFood(){
	var nameoffood = $('#foodname').val();
	if(nameoffood){
		$.ajax({
    	url: "/api/user/food",
		method: "POST",
		contentType: "application/json",
		data: JSON.stringify({"foodname": nameoffood})
		}).done(function(response){
			console.log(response);
			refreshPage();
			$('#foodname').val('');
		});
	}else{
		alert('Vui lòng điền tên món ăn');
	};
};
$(document.body).on('keydown', $('inputFood'), function(event) {
  	if(event.keyCode == 13) {
    	submitFood();
  	}
});
$('form').on('click', '#createFood', function(){
	submitFood();
});

//======================================================================
//======================================================================
//======================================================================
//======================================================================

//Today List Food Table
function addFoodTable(nameoffood, idFood){
	var tableContent = [];
	tableContent += '<tr data-id="' + idFood + '" class="col s12 l3">';
    tableContent += '<td class="card-title"> ' + nameoffood + '</td>';
    tableContent += '</tr>';
    $('#orderTable tbody').append(tableContent);   
};

var sendListFood = [];

//Add food to Today List Food Table both client and server side
$("#listFood").on('click', '.addFoodToday', function(){
	var id = $(this).parent().parent().parent().data("id");
	var foodNameToday = $(this).parent().prev().children().text();
	var countFood = 0;
	$('#orderTable tr').each(function(){
		if (id === $(this).data("id")){
			countFood += 1;
		}else{countFood += 0};
	});
	if(countFood === 0){
		$("*[data-id=" + id + "] > div").css("background-color", "#26a69a");
	addFoodTable(foodNameToday, id);
	$.ajax({
    	url: "/api/user/food/today",
		method: "POST",
		contentType: "application/json",
		data: JSON.stringify({"foodNameToday": foodNameToday, "idFoodToday": id})
		}).done(function(response){
			console.log(response);
		});
	}
});
//Delete food from Today List Food Table both client and server side
$("#listFood").on('click', '.clearFoodToday', function(){
	var id = $(this).parent().parent().parent().data("id");
	$("*[data-id=" + id + "] > div").css("background-color", "#546e7a");
	$('#orderTable tr').each(function(){
		if (id === $(this).data("id")){
			$(this).remove();
			$.ajax({
		    	url: "/api/user/food/today",
				method: "DELETE",
				contentType: "application/json",
				data: JSON.stringify({"idFoodToday": id})
				}).done(function(response){
					console.log(response);
			});
		};
	});
});

//======================================================================
//======================================================================
//======================================================================
//======================================================================
//======================================================================

$("#orderArea").on('click', '#sendOrder', function(){
	if($('#orderTable tr').length === 1){
		return alert('Vui lòng chọn món ăn');
	}else{
		if (confirm("Bạn có chắc muốn gửi danh sách món ăn này cho nhà hàng?")) {
	        $.ajax({
			url: "/api/user/sendCustomerOrder",
			method: "POST"
			}).done(function(response){
				console.log(response);
				refreshPage();
				alert("Bạn đã gửi thông tin thành công !!!");
			}); 
	    }
	    return false;
	};
});