var path = require('path');

module.exports = function(app) {
  // Insert routes below
  app.use('/api/user', require('./api/user'));
  app.use('/api/auth', require('./api/auth'));//route to /api/auth
}