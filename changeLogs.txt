17/10/2016
- fix bug: add 1 food more than 1 time in the Today List Table. Now 1 food can be added only one. Same with delete food from Today List Table.
- When delete food from All Food List: alert to ask user confirm/ and food from Today List Table is removed as well both client and server
- Refresh page still keep last choices from user
- Ask user when empty field, send form, delete item