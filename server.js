//Set up variable, required modules
var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var session = require('express-session');
var parseurl = require('parseurl');
var cookieParser = require('cookie-parser');
var auth = require('./api/auth/auth.service');
var mongoose = require('mongoose');
//==========================================
//Set the port for API
var port = process.env.PORT || 4000;
//==========================================

//use bodyParser to get data from post request
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(cookieParser('cuongpt', {maxAge: 120}));
app.use(session({
  secret: "cuongpt"
}));

//log request to the console
app.use(morgan('dev')); //color output

//handle CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
//==========================================


//mongoose setup database where to save
mongoose.connect('mongodb://admin:fooderadmin@ds047146.mlab.com:47146/fooder')

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'DB connection error: '));
db.once('open', function() {
  console.log('DB connection success! ');
});//check if connect success to Dbmongo
//==========================================


//====Routes====
app.use('/admin', auth.hasRole('admin'), express.static(__dirname + "/client/admin"));
app.use('/customer', auth.hasRole('customer'), express.static(__dirname + "/client/customer"));
app.use('/restaurant', auth.hasRole('restaurant'), express.static(__dirname + "/client/restaurant"));
app.use('/', express.static(__dirname + "/client"));
require('./routes')(app);

//====Start server====
app.listen(port);
console.log(port + " is connected"); 

